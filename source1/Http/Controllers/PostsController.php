<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Contracts\PostInterface;


class PostsController extends Controller
{

	private $postInterface;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(PostInterface $postInterface)
	{
		$this->postInterface = $postInterface;
		//$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(PostInterface $postInterface)
	{

		if (Auth::check())
		{
			return response()->json($this->postInterface->getAllPost());
		}

		return response()->json(['error'=>'please Login']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(PostRequest $request, PostInterface $postInterface)
	{
		$postInterface->createPost($request->all());

		return response()->json(array('success'=>true, 'message'=>'Post Added'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update($id, PostRequest $request, PostInterface $postInterface)
	{
		$postInterface->putSinglePost($id, $request->all());

		return response()->json(array('success'=> true, 'message'=>'Post Edited'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id, PostInterface $postInterface)
	{
		$postInterface->deleteSinglePost($id);

		return response()->json(array('success'=> true, 'message'=>'Post Deleted'));
	}
}