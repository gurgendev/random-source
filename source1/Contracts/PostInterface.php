<?php

namespace App\Contracts;

interface PostInterface{

    public function getIndexPost();

    public function getAllPost();
    
    public function createPost($request);
    
    public function putSinglePost($id,$request);
    
    public function deleteSinglePost($id);

}