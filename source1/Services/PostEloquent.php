<?php

namespace App\Services;

use App\Contracts\PostInterface;
use App\Models\Post;
use App\Http\Requests;
use Illuminate\Http\Response;
use Auth;


class PostEloquent implements PostInterface {
    
    public function getIndexPost(){

        return Post::all();
    }
    
    public function getAllPost(){

        return Post::where('user_id', Auth::id())->orderBy('id', 'desc')->get();
    }

    public function createPost($request){
        
        $request['user_id'] = Auth::id();
        return  Post::create($request);
    }

    public function putSinglePost($id, $request){

        $article = Post::find($id);
        return $article->update($request);
    }

    public function deleteSinglePost($id){

        $article = Post::find($id);
        return $article->delete();
    }
    
}