<?php

namespace App\Controller;

use App\Entity\Statistics;
use App\Form\StatisticsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StatisticsController extends Controller
{
    /**
     * @Route("/statistics", name="statistics")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(StatisticsType::class, new Statistics());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em=$this->getDoctrine()->getManager();

            $em->persist($data);
            $em->flush();
            return $this->redirectToRoute('promotions');
        }

        return $this->render('statistics/create.html.twig', [
            'controller_name' => 'StatisticsController',
            'form'=>$form->createView()
        ]);
    }
}
