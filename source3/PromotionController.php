<?php

namespace App\Controller;

use App\Entity\Promotion;
use App\Form\PromotionType;
use App\Service\IPromotionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PromotionController extends Controller
{
    private $promotionService;

    public function __construct(IPromotionService $promotionService)
    {
        $this->promotionService = $promotionService;
    }

    /**
     * @Route("/promotions", name="promotions")
     */
    public function index()
    {
        return $this->render('promotion/index.html.twig', [
            'promotions' => $this->promotionService->getPromotionsStatistics(),
        ]);
    }

    /**
     * @Route("/promotion/create", name="promotion.create")
     */
    public function create(Request $request)
    {
        $promotion = new Promotion();
        $promotion->setCreatedAt(new \DateTime('now'));

        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $data = $form->getData();

                if(count($data->getPromotionProducts()) == 0){
                    throw new \InvalidArgumentException('Products form is empty. Please add products');
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($promotion);

                foreach ($data->getPromotionProducts() as $productData) {
                    $product = $productData;
                    $product->setPromotion($promotion);
                    $em->persist($product);
                }
                $em->flush();
                $this->addFlash('success', 'Promotion "' . $promotion->getName() . '" successfully created!');

                return $this->redirectToRoute('promotions');
            }catch(\InvalidArgumentException $ex){
                $this->addFlash('error',$ex->getMessage());
            } catch (\Exception $ex) {
                $this->addFlash('error','Unable to Create Promotion');
            }
        }

        return $this->render('promotion/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
