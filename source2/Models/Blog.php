<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $fillable = [
        'title',
        'slug',
        'description',
        'active',
        'most_popular',
        'editor_choice',
        'meta_title',
        'meta_description',
        'published_at',
    ];

    /************ Relations ***********/

    /**
     * The image that belong to the post.
     */
    public function image()
    {
        return $this->hasMany('App\Models\ImageBlog','post_id','id');
    }

    /**
     * The type that belong to the post.
     */
    public function type()
    {
        return $this->belongsToMany('App\Models\Type','blog_type','blog_id','type_id');
    }

    /**
     * The category that belong to the post.
     */
    public function category()
    {
        return $this->belongsToMany('App\Models\Category','blog_category','blog_id','category_id');
    }

}
