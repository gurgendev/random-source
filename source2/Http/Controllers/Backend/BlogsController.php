<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepositoryInterface;
use App\Interfaces\Backend\BlogsServiceInterface;


class BlogsController extends Controller
{
    private $blogsService;


    /**
     * BlogsController constructor.
     *
     * @param UserRepositoryInterface $user
     * @param UserRepositoryInterface $user
     */
    public function __construct(UserRepositoryInterface $user, BlogsServiceInterface $blogsService)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->blogsService = $blogsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BlogsServiceInterface $blogsService)
    {
        $posts = $blogsService->allPosts();

        return view('Backend.blogs.index',
            [
                'posts' => $posts,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BlogsServiceInterface $blogsService)
    {
        $datas = $blogsService->createPage();

        return view('Backend.blogs.create',
            [
                'products' => $datas[0],
                'types' => $datas[1],
                'categories' => $datas[2],
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BlogsServiceInterface $blogsService)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'type' => 'required',
            'category' => 'required',
            'published_at' => 'required',
        ]);

        $data = $request->all();

        $images = $request->file('image');
        if (null !== $images) {
            $data['post_images'] = $this->get_image(images);
        } else {
            $data['post_images'] = null;
        }

        $createNewPost = $blogsService->storePost($data);

        return \Redirect::to('admin/posts')->with('success', 'Great! Post created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug, BlogsServiceInterface $blogsService)
    {
        //$blog = $blogsService->singlePost($slug);

        return view('Backend.blogs.single',
            [
                'blog' => 'This is single blog post',
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, BlogsServiceInterface $blogsService)
    {
        $post = $blogsService->getSinglePost($id);
        $datas = $blogsService->createPage();

        return view('Backend.blogs.edit',
            [
                'post' => $post,
                'products' => $datas[0],
                'types' => $datas[1],
                'categories' => $datas[2],
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, BlogsServiceInterface $blogsService)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
            'published_at' => 'required',
        ]);

        $data = $request->all();

        $images = $request->file('image');
        if (null !== $images) {
            $data['post_images'] = $this->get_image(images);
        } else {
            $data['post_images'] = null;
        }

        $createNewPost = $blogsService->getUpdateSinglePost($id, $data);

        return \Redirect::to('admin/posts')->with('success', 'Great! Post created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, BlogsServiceInterface $blogsService)
    {
        $blogsService->deletePost($id);
        return \Redirect::to('admin/posts')->with('success', 'Great! Deleted successfully.');
    }

    /**
     * Remove image
     *
     */
    public function imageDelete(Request $request, BlogsServiceInterface $blogsService)
    {
        if ($request->isXmlHttpRequest()) {
            $data = $request->all();

            $product = $blogsService->deleteImagePost($data);

            if ($product->id) {
                $response = true;
            } else {
                $response = false;
            }

            return response()->json(['success' => $response]);
        }
    }

    /**
     * Sort the images of products
     *
     */
    public function imageSort(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $data = $request->all();

            $res = $this->blogsService->imagesSort($data, $data['sort']);

            return response()->json(['success' => $res]);
        }
    }

    /**
     * filter digits
     *
     */
    public function get_numerics($str)
    {
        preg_match_all('/\d+/', $str, $matches);
        return $matches[0];
    }

    /**
     * get images
     *
     */
    private function get_image($images)
    {
        $destinationPath = public_path('uploads/posts');

        foreach ($images as $key => $image) {
            $ext = $image->getClientOriginalExtension();
            $image_name = uniqid() . time() . '.' . $ext;
            $pathImg = $destinationPath . '/' . $image_name;
            $image->move($destinationPath, $image_name);
            $data['post_images'][$key] = $image_name;
        }

        return $data['post_images'];
    }

}
