<?php namespace App\Services\Backend;

use App\Interfaces\Backend\BlogsServiceInterface;
use Illuminate\Contracts\Auth\Guard;
use Carbon\Carbon;
use App\Models\Blog;
use App\Models\Type;
use App\Models\Category;
use App\Models\Product;
use App\Models\ImageBlog;


class BlogService implements BlogsServiceInterface
{
    private $blog;

    private $imageBlog;

    private $type;

    private $category;

    private $product;

    private $now;

    public function __construct(Blog $blog, Product $product, ImageBlog $imageBlog, Type $type, Category $category)
    {
        $this->blog = $blog;
        $this->imageBlog = $imageBlog;
        $this->type = $type;
        $this->category = $category;
        $this->product = $product;
        $this->now = Carbon::now();
    }

    /**
     * Get all posts.
     */
    public function allPosts()
    {
        return $this->blog->with('image', 'type', 'category')->orderBy('published_at', "desc")->paginate(10);
    }

    /**
     * Get single post
     */
    public function singlePost($slug)
    {

    }

    /**
     * Create Page datas
     */
    public function createPage()
    {
        $products = $this->product->get();

        $types = $this->type->get();

        $categories = $this->category->get();

        return [$products, $types, $categories];
    }

    /**
     * Store Page datas
     */
    public function storePost($data)
    {
        $types = (isset($data['type']) && !empty($data['type']) ? $data['type'] : []);
        $categories = (isset($data['category']) && !empty($data['category']) ? $data['category'] : []);
        unset($data['_token']);
        unset($data['image']);
        unset($data['type']);
        unset($data['category']);


        //$data['uuid'] = \Uuid::generate(4)->string;
        //$data['uuid'] = $this->generateRandomString();
        $data['slug'] = $this->createSlug($data['title']);

        $data['active'] = ((isset($data['active']) && $data['active'] == 'on') ? true : false);
        $data['most_popular'] = ((isset($data['most_popular']) && $data['most_popular'] == 'on') ? true : false);
        $data['editor_choice'] = ((isset($data['editor_choice']) && $data['editor_choice'] == 'on') ? true : false);
        $data['published_at'] = Carbon::parse($data['published_at'])->format('Y-m-d') . ' ' . $this->now->toTimeString();

        $post = $this->blog->create($data);

        $post->type()->sync($types);
        $post->category()->sync($categories);

        if (null != $data['post_images']) {
            foreach ($data['post_images'] as $key => $image) {
                $imageBlogVal['name'] = $image;
                $imageBlogVal['post_id'] = $post->id;
                $imageBlogVal['sort'] = $key + 1;
                $this->imageBlog->create($imageBlogVal);
            }
        }
        return true;
    }

    /**
     * Get single post
     */
    public function getSinglePost($id)
    {
        return $this->blog->where('id', $id)->with(array('image' => function ($query) {
            $query->orderBy('sort', 'ASC');
        }))->first();
    }

    /**
     * Update single post
     */
    public function getUpdateSinglePost($id, $data)
    {
        $types = (isset($data['type']) && !empty($data['type']) ? $data['type'] : []);
        $categories = (isset($data['category']) && !empty($data['category']) ? $data['category'] : []);
        $images = $data['post_images'];

        unset($data['_token']);
        unset($data['_method']);
        unset($data['type']);
        unset($data['category']);
        unset($data['post_images']);
        unset($data['image']);
        unset($data['q']);

        $data['slug'] = $this->createSlug($data['title']);

        $data['active'] = ((isset($data['active']) && $data['active'] == 'on') ? true : false);
        $data['most_popular'] = ((isset($data['most_popular']) && $data['most_popular'] == 'on') ? true : false);
        $data['editor_choice'] = ((isset($data['editor_choice']) && $data['editor_choice'] == 'on') ? true : false);
        $data['published_at'] = Carbon::parse($data['published_at'])->format('Y-m-d') . ' ' . $this->now->toTimeString();

        $post = $this->blog->where('id', $id)->update($data);
        if ($post) {
            $updatedPost = $this->blog->where('id', $id)->with('image', 'type', 'category')->first();

            $updatedPost->type()->sync($types);
            $updatedPost->category()->sync($categories);

            if (null != $images) {
                foreach ($images as $key => $image) {
                    $imagePost['name'] = $image;
                    $imagePost['post_id'] = $updatedPost['id'];
                    $imagePost['sort'] = null;
                    $this->imageBlog->create($imagePost);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete single post
     */
    public function deletePost($id)
    {
        return $this->blog->where('id', $id)->delete();
    }

    /**
     * Delete image of post
     */
    public function deleteImagePost($data)
    {
        return $this->blog->where('id', $data['itemId'])->with(array('image' => function ($query) use ($data) {
            $query->where('id', $data['imageId'])->delete();
        }))->first();
    }

    /**
     * Get images sort of posts
     */
    public function imagesSort($data, $newSort)
    {
        foreach ($newSort as $key => $id) {

            $this->imageBlog->where('id', (int)$id)->update(['sort' => (int)$key]);
        }

        return true;
    }

    /**
     * Create slug
     */
    private function createSlug($str)
    {
        return mb_strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), $this->removeAccent($str)));
    }

    /**
     * Remove
     */
    private function removeAccent($str)
    {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        return mb_convert_encoding(str_replace($a, $b, $str), 'UTF-8', mb_detect_encoding(str_replace($a, $b, $str)));
    }

    /**
     * Generate Random String
     */
    private function generateRandomString($length = 7)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
