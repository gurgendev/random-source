<?php namespace App\Interfaces\Backend;

interface BlogsServiceInterface {

    public function allPosts();

    public function singlePost($slug);

    public function createPage();

    public function storePost($data);

    public function getSinglePost($uuid);

    public function getUpdateSinglePost($id,$data);

    public function imagesSort($data,$newSort);

    public function deletePost($id);
}
